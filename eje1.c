#include <stdio.h>
#include <stdlib.h>

int main(void){
	//declaracion de array
	int asientos[5][5];
	int fil,col;
	//declaracion de precios
	double fila1 = 5.0;
	double fila2 = 3.5;
	double fila3 = 2.5;
	//declaracion de variable para las ganancias
	double ganancias = 0.0; 
	//otras variables
	int opcion;
	//llenado del array
	for(fil = 0; fil<5; fil++){
		for(col = 0; col<5; col++){
			asientos[fil][col] = 0;
		}
	}
	
	//opciones del menu
	printf("Ingrese el numero de la opcion que desea realizar\n 1.-Vender Entradas\n 2.-Mostrar ganancias\n");
	scanf("%d", &opcion);
	//menu
	do{
		if (opcion==1){//sistema de ventas de entradas
			int fila=0;
			int columna=0;
			printf("Elija la fila del asiento que desea\n");//elecion de fila
			scanf("%d", &fila-1);
			printf("Elija el asiento que desea\n");//elecion de columna
			scanf("%d", &columna-1);
			
			if(asientos[fila][columna] == 0){//condicional cuando el asiento este disponible
				printf("Asiento vendido con exito\n");
				asientos[fila][columna] = 1;
				if(fila == 0){
					ganancias = ganancias + fila1;
				}
				if(fila > 0 && fila<4){
					ganancias = ganancias + fila2;
				}
				if(fila == 4){
					ganancias = ganancias + fila3;
				}
			}else if (asientos[fila][columna] == 1){//condicional cuando el asiento este vendido
				printf("El asiento ya fue vendido\n");
			}
		}
		if (opcion==2){//mostrar ganancias
			printf("Las ganancias son de : $ %d",ganancias);
		}
		
		
		printf("\nIngrese el numero de la opcion que desea realizar\n 1.-Vender Entradas\n 2.-Mostrar ganancias\n");
		scanf("%d", &opcion);//menu ya dentro del bucle
	}while(opcion==1 || opcion == 2);//cerramos el menu
	
	return 0;
}

